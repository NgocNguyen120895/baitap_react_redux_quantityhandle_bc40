import './App.css';
import ButtonComponent from './btn_redux/ButtonComponent';

function App() {
  return (
    <div className="App">
      <ButtonComponent></ButtonComponent>
    </div>
  );
}

export default App;
