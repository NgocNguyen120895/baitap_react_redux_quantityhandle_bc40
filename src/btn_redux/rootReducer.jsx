import { combineReducers } from "redux";
import { numberReducer } from "./buttonReducer";

export const rootReducerNumber = combineReducers({
    number: numberReducer
})