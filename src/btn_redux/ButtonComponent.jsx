import React, { Component } from 'react'
import { connect } from 'react-redux'

class ButtonComponent extends Component {
    render() {
        return (
            <div>
                <button onClick={() => { this.props.quantityHandle(false)}}>-</button>
                <span>{this.props.quantity}</span>
                <button onClick={() => { this.props.quantityHandle(true)}}>+</button>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        quantity: state.number.initNumber,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        quantityHandle: (tangGiam) => {
            const action = {
                type: "QTY_CHANGE",
                tangGiam,
            }
            dispatch(action)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ButtonComponent)