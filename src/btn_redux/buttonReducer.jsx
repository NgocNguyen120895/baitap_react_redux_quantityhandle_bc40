
let initValue = {
    initNumber: 8888,
}

export const numberReducer = (state = initValue, action) => {
    switch (action.type) {
        case "QTY_CHANGE": {
            if (action.tangGiam) {
                state.initNumber++
            } else { 
                state.initNumber--
            }
            return {...state}
        }
        default: {
            return state
        }
    }
}
